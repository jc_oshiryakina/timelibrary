<?php
// HTTP
define('HTTP_SERVER', 'http://timelibrary.widev.org/');

// HTTPS
define('HTTPS_SERVER', 'http://timelibrary.widev.org/');

// DIR
define('DIR_APPLICATION', '/var/www/user/data/www/timelibrary.widev.org/catalog/');
define('DIR_SYSTEM', '/var/www/user/data/www/timelibrary.widev.org/system/');
define('DIR_IMAGE', '/var/www/user/data/www/timelibrary.widev.org/image/');
define('DIR_LANGUAGE', '/var/www/user/data/www/timelibrary.widev.org/catalog/language/');
define('DIR_TEMPLATE', '/var/www/user/data/www/timelibrary.widev.org/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/user/data/www/timelibrary.widev.org/system/config/');
define('DIR_CACHE', '/var/www/user/data/www/timelibrary.widev.org/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/user/data/www/timelibrary.widev.org/system/storage/download/');
define('DIR_LOGS', '/var/www/user/data/www/timelibrary.widev.org/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/user/data/www/timelibrary.widev.org/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/user/data/www/timelibrary.widev.org/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'watch');
define('DB_PASSWORD', '673462srg');
define('DB_DATABASE', 'watch');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
